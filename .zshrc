# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt autocd
# unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install

# Shell hooks

# brew
[ -e /home/linuxbrew ] && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# zoxide
hash zoxide 2> /dev/null && eval "$(zoxide init --cmd y zsh)"

# starship prompt
hash starship 2> /dev/null && eval "$(starship init zsh)"

# rtx
if [ -e ~/.local/share/mise/bin/mise ]; then
  eval "$(~/.local/share/mise/bin/mise activate zsh)";
else
  hash mise 2> /dev/null && eval "$(mise activate zsh)"
fi

# fzf initialization
hash fzf 2> /dev/null && source <(fzf --zsh)

# atuin
hash atuin 2> /dev/null && eval "$(atuin init zsh --disable-up-arrow)"

# direnv
hash direnv 2> /dev/null && eval "$(direnv hook zsh)"

# environment variables
export PATH="/home/$USER/.local/bin":$PATH
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
export EDITOR="$(which vim)"
export TERM="xterm-256color"
export VAULT_ADDR="https://vault.ops.zeit.de/"
export GITHUB_USER=BenediktFloeser
export TERRAGRUNT_PROVIDER_CACHE=1

# aliases

# file listing
if hash eza 2> /dev/null; then
  alias ls="eza"
  alias ll="eza -l --icons -s type"
  alias la="eza -lah --icons -s type"
  alias lt="eza -lT --icons -s type"
  alias lr="eza -ls modified --icons"

else
  alias ll="ls -l"
  alias la="ls -lah"
  alias lr="ls -ltr"
fi

# updating aliases
alias upd="topgrade --disable gcloud node containers"
alias zed-upd="cd ~/Code/private/zed && git pull --recurse-submodules && /bin/sh ~/Code/private/zed/script/install-linux"

# locations and common files
alias zrc="$EDITOR ~/.zshrc"
alias vrc="$EDITOR ~/.config/nvim/init.vim"
alias arc="$EDITOR ~/.config/alacritty/alacritty.toml"

# common applications
alias vimd="vim -c VimwikiMakeDiaryNote"
alias zj="/home/linuxbrew/.linuxbrew/bin/zellij --layout /home/benedikt/.config/zellij/layout.kdl"
alias just="just --unstable"
alias helix=hx
alias xo=xdg-open

# git
alias gcl="git clone"
alias gs="git status"
alias gsh="git show"
alias gd="git diff"
alias ga="git add"
alias gaa="git add -A"
alias gc="git commit"
alias gcm="git commit -m"
alias gm="git merge"
alias grb="git rebase"
alias grs="git reset"
alias grest="git restore"
alias gps="git push"
alias gpl="git pull"
alias gref="git reflog"
alias gl="git log --all --decorate --graph --oneline"

# k8s
alias kc="kubectx"
alias kn="kubens"
alias k="kubectl"
alias kd="kubectl run -it --rm --restart=Never benedikt-debug --image=praqma/network-multitool sh"

# ytfzf
alias yt="ytfzf --force-youtube --pages=2"
alias ytr="ytfzf --force-youtube --sort"
alias ytc="ytfzf --force-youtube --type=channel"
alias ytl="ytfzf --force-youtube -l --type=playlist"
alias ytm="ytfzf --force-youtube -m --pages=2"

# dotfile management
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
unsetopt complete_aliases

# docker-compose
alias dc="docker compose"
alias pc=podman-compose

# terraform
alias tf=tofu
alias tg=terragrunt

# git 
alias gu=gitui
alias lg=lazygit
alias gl="git log --all --decorate --graph --oneline"

# zsh setup

if type brew &>/dev/null; then
  [[ -f ~/.zsh/catppuccin_frappe-zsh-syntax-highlighting.zsh ]] && source ~/.zsh/catppuccin_frappe-zsh-syntax-highlighting.zsh
  source /home/linuxbrew/.linuxbrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  source /home/linuxbrew/.linuxbrew/share/zsh-autosuggestions/zsh-autosuggestions.zsh
fi

# Autosuggestions coloring stuff
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#505050"


# Functions

mcd () { 
    [ "$1" ] && mkdir "$1" && cd "$1" || return
}

# Used to lint Containerfiles. Expects the path to one as argument
slim-lint () {
  if [ -z "$1" ]; then
    echo "No target was given!"
    return 1
  fi

  if ! hash slim 2> /dev/null; then 
    echo "slim is not available in PATH. Is it installed?"
    return 1
  fi

  slim lint --target "$1" > /dev/null && \
  yq -p json -o yaml slim.report.json && \
  rm slim.report.json
}

ks () {
  if [[ -z "$STARSHIP_K8S_CONTEXT" ]]; then
    export STARSHIP_K8S_CONTEXT=true
  else 
    unset STARSHIP_K8S_CONTEXT
  fi
}

ytd () {
  if [[ -z $1 ]]; then
    echo "No URL given!"
  else
    mpv --ytdl --script-opts="ytdl_path=yt-dlp" --ytdl-raw-options=format="bestvideo[height<=720][ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best" "$1"
  fi
}

# Brew Autocompletion
if type brew &>/dev/null
then
  FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"

  autoload -Uz compinit
  compinit
fi

[ -e ~/.cargo/env ] && source ~/.cargo/env
