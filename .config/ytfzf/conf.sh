enable_hist=1
ytdl_path="/usr/bin/yt-dlp"
YTFZF_PREF="bestvideo[height<=?1080]+bestaudio/best"

external_menu() {
    rofi -dmenu -theme Arc-Dark -font "FiraCode 13" -width 90% -p "$1"
}

